fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data));

// Add POST data
document.querySelector("#form-add-post").addEventListener('submit', (e) => {
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts', {
        method : 'POST',
        body : JSON.stringify({
            title : document.querySelector('#txt-title').value,
            body : document.querySelector('#txt-body').value,
            userId : 1,
        }),
        headers: {'Content-type' : 'application/json; charset=UTF-8'}
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        document.querySelector("#form-add-post").reset();
    })
});

// Show POSTS data
const showPosts = (posts) => {
    let postEntries = "";

    posts.forEach(post => {
        postEntries += `
            <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onClick="editPost('${post.id}')">Edit</button>
            <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        `;
    });
    document.querySelector("#div-post-entries").innerHTML = postEntries
};

// Edit POST data
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;

    document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// Update POST
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method : 'PUT',
        body : JSON.stringify({
            id : document.querySelector('#txt-edit-id').value,
            title : document.querySelector('#txt-edit-title').value,
            body : document.querySelector('#txt-edit-body').value,
            userId : 1
        }),
        headers : {'Content-type' : 'application/json; charset=UTF-8'}
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert("Item updated");
        
        document.querySelector("#form-edit-post").reset();
        document.querySelector('#btn-submit-update').setAttribute('disabled', true);
    });
});

// Delete POST
const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method : 'DELETE'
    })
    .then((response) => response.json())
    .then((data) => {
        document.querySelector(`#post-${id}`).remove();
    })
};